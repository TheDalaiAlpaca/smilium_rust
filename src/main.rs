use std::{process, thread, time::Duration};
use crossterm::{terminal, cursor, input, InputEvent, KeyEvent, RawScreen};

struct Picture {
    displayed_chars: Vec<String>,
    style_chars:     Vec<String>,
    width:           u16,
    height:          u16,
}

impl Picture {
    fn new() -> Picture {
        Picture { 
            displayed_chars: vec![String::from("")],
            style_chars:     vec![String::from("")],
            width:           0,
            height:          0,
        }
    }

    fn to_viewport(&self) -> String {
        let terminal             = terminal();
        let (terminal_width,
             terminal_height)    = terminal.terminal_size();
        let mut viewport         = String::from("").to_owned();
        let mut right_margin_row = String::from("").to_owned();
        let bottom_area          = fill_area(terminal_height - self.height, terminal_width, ' ');

        // Create right_margin_row
        for _i in self.width..terminal_width {
            right_margin_row.push(' ');
        }
        right_margin_row.push_str("\r\n");
       
        let mut pos: usize = 0;
        for _i in 0..self.height as usize {
            for _j in 0..self.width as usize {
                viewport.push_str(&self.style_chars[pos].to_owned());
                viewport.push_str(&self.displayed_chars[pos].to_owned());
                pos += 1;
            }
        
            viewport.push_str(&right_margin_row);
        }

        viewport.push_str(&bottom_area);

        viewport
    }
}

fn fill_area(rows: u16, cols: u16, character: char) -> String {
    let mut area: String = String::from("").to_owned();
    let mut row:  String = "".to_owned();

    for _i in 0..cols {
        row.push(character);
    }

    for _i in 0..rows {
        area.push_str(&row);
        area.push_str("\r\n");
    }

    area
}

fn main() {
    let cursor_char:      char    = 'X';
    let terminal                  = terminal();
    let mut cursor                = cursor();
    let mut cursor_x:     u16     = 0;
    let mut cursor_y:     u16     = 0;
    let input                     = input();
    let mut async_stdin           = input.read_async();
    let (terminal_width,
         terminal_height)         = terminal.terminal_size();
//  let viewport:         String  = fill_area(terminal_height, terminal_width, ' ');
    let mut picture               = Picture {
                                        displayed_chars: vec![
                                            String::from("D"),
                                            String::from("A"),
                                            String::from("F"),
                                            String::from("T"),
                                        ],
                                        style_chars: vec![
                                            String::from(""),
                                            String::from(""),
                                            String::from(""),
                                            String::from(""),
                                        ],
                                        width: 2,
                                        height: 2,
                                    };
    let viewport                  = picture.to_viewport();

    print!("{}", viewport);
    cursor.goto(cursor_x, cursor_y).unwrap();

    let _screen = RawScreen::into_raw_mode().unwrap();
    loop {
        // Try to get the next input event
        if let Some(key_event) = async_stdin.next() {
            match key_event {
                InputEvent::Keyboard(event) => {
                    match event {
                        KeyEvent::Esc => {
                            println!("\rProgram closing");
                            break;
                        },
                        KeyEvent::Right    => {
                            cursor.move_right(1);
                            if cursor_x < terminal_width { cursor_x += 1; }
                        },
                        KeyEvent::Left     => {
                            cursor.move_left(1);
                            if cursor_x > 0 { cursor_x -= 1; }
                        },
                        KeyEvent::Up       => {
                            cursor.move_up(1);
                            if cursor_y > 0 { cursor_y -= 1; }
                        },
                        KeyEvent::Down     => {
                            cursor.move_down(1);
                            if cursor_y < terminal_height { cursor_y += 1; }
                        },
                        KeyEvent::End      => {
                            cursor.goto(terminal_width, cursor_y).expect("Out of range");
                            cursor_x = terminal_width;
                        },
                        KeyEvent::Home     => {
                            cursor.goto(0, cursor_y).expect("Out of range");
                            cursor_x = 0;
                        },
                        KeyEvent::PageUp   => {
                            cursor.goto(cursor_x, 0).expect("Out of range");
                            cursor_y = 0;
                        },
                        KeyEvent::PageDown => {
                            cursor.goto(cursor_x, terminal_height).expect("Out of range");
                            cursor_y = terminal_height;
                        },
                        _                  => { },
                    }
                }
                _ => { },
            }
        }
        thread::sleep(Duration::from_millis(50));
    }

    let _screen = RawScreen::disable_raw_mode().expect("Fucked up your terminal, so sorry!");
    print!("\r");

    process::exit(0);
}
